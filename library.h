#ifndef _LIBRARY_H
#define _LIBRARY_H

#include "date.h"

#define USER_DB  "user.db"
#define BOOKS_DB  "books.db"
#define BOOKCOPY_DB "bookcopy.db"
#define ISSURECORD_DB "issuerecord.db"


#define ROLE_OWNER "owner"
#define ROLE_LIBRARIAN "librarian"
#define ROLE_MEMBER "member"
#define STATUS_AVAIL "available"
#define STATUS_ISSUED "issued"


#define PAY_TYPE_FESS "fess"
#define PAY_TYPE_FINE "fine"

#define FINE_PER_DAY             5
#define BOOK_RETURN_DAYS         7
#define MEMBERSHIP_MONTH_DAYS   30

#define EMAIL_OWNER     "akshay@gmail.com"




typedef struct user {
    int id;
    char name[40];
    char email[40];
    char phone[15];
    char password[12];
    char role[20];
}user_t;

typedef struct book {
    int id;
    char name[50];
    char author[50];
    char subject[40];
    double price;
    char isbn[20];
    }book_t;

typedef struct bookcopy {
     int id;
     int bookid;
     int rack;
     char status[20];
}bookcopy_t;

typedef struct issuerecord {
    int id;
    int copyid;
    int memberid;
    date_t isssue_date;
    date_t return_duedate;
    date_t return_date;
    double fine_amount;
}issuerecord_t;

 typedef struct payment {
    int id;
    int memberid;
    double amount;
    char type[10];
    date_t tx_time;
    date_t next_pay_duedate;

}payment_t;

// user function
void user_accept(user_t *u);
void user_display(user_t *u);

// book function
void book_accept(book_t *b);
void book_display(book_t *b);

// bookcopy function 
void bookcopy_accept(bookcopy_t *bc);
void bookcopy_display(bookcopy_t *bc);

// issuerecord function
void issuerecord_accept(issuerecord_t *ic);
void issuerecord_display(issuerecord_t *ic);

// payment function
void payment_accept(payment_t *p);
void payment_display(payment_t *p);


// OWNER FUNCTION
 void owner_area(user_t *u);
 void appoint_librarian();


//librarian function
void librarian_area(user_t *u);
void add_member();
void add_book();
void book_edit_by_id();
void bookcopy_add();
void bookcopy_checkavail_details();
void bookcopy_issue();
void bookcopy_checkstatus(int bookcopy_id, char status[]);
void display_issued_bookcopies(int member_id);
void bookcopy_return();




//member function
void member_area(user_t *u);
void bookcopy_checkavail();

//common function
 void sign_in();
 void sign_up();
 void edit_profile(user_t *u);
 void change_password(user_t *u);
 int get_next_user_id();
 int get_next_book_id();
 int get_next_bookcopy_id();
 int get_next_issurecord_id();


 void user_add(user_t *u);
 int user_find_by_email(user_t *u, char email[]);
 
void book_find_by_name(char name[]);

#endif



















